import { LightningElement, track } from 'lwc';

import getDataWS from '@salesforce/apex/Test.getDataWS';
import getDataSF from '@salesforce/apex/Test.getDataSF';


import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    {label:'Title',fieldName:'title__c', type: 'text'},
    {label:'Author Fullname',fieldName:'author_fullname__c', type: 'text'},
    {label:'Thumbnail',fieldName:'thumbnail__c', type: ''},
    {label:'Selftext',fieldName:'selftext__c', type: 'text'},
    {label:'Created',fieldName:'created__c', type: 'dateTime', sortable: true},
    {
        type:"button",
        fixedWidth: 150,
        typeAttributes: {
            label: 'Delete',
            name: 'delete',
            variant: 'brand'
        }
    }
];

export default class Test extends LightningElement {
    @track isLoading = false;
    @track dataSF;
    @track cols = columns;
    @track sortDirection = 'asc';

    connectedCallback(){
        this.getDataWSJS();
    }

    getDataWSJS(){
        this.isLoading = true;

        getDataWS({})
        .then(result =>{
            this.getDataSFJS();
        })
        .catch(error=>{
            new ShowToastEvent({
                title: 'Error',
                message: error,
                variant: 'error'
            })
        });

    }

    getDataSFJS(){
        getDataSF({order: this.sortDirection})
        .then(result =>{
            this.dataSF = result;
            this.isLoading = false;
        })
        .catch(error=>{
            new ShowToastEvent({
                title: 'Error',
                message: error,
                variant: 'error'
            })
        });
    }

    doSorting(event) {
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortDirection);
    }

    sortData(direction) {
        let parseData = JSON.parse(JSON.stringify(this.dataSF));
        // Return the value stored in the field
        let keyValue = (a) => {
            return a['created__c'];
        };
        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1: -1;
        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.dataSF = parseData;
    } 

    deleteRecordJS(event){
        this.isLoading = true;
        deleteRecord(event.detail.row.Id)
            .then(() => {
                this.getDataSFJS();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: error,
                        variant: 'error'
                    })
                );
            });

    }
}